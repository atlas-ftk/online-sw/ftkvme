#ifndef VME_SPY_BUFFER
#define VME_SPY_BUFFER

/*
 *  Functions used for spy buffer access using VME block transfer
 */

#include <string>
#include <vector>
#include "VMEInterface.h"

//Including the base class
#include "ftkcommon/SpyBuffer.h"


namespace daq { 
  namespace ftk {

    class vme_spyBuffer : public daq::ftk::SpyBuffer{

    protected:
      
      unsigned int m_spy_status_reg_VME_address;
      unsigned int m_spy_RAM_VME_base_address;
      VMEInterface *m_vme_interface;

      //SpyStatus information is stored in m_spyStatus of the base class

      //An enforced spyDimension value
      uint32_t m_userSpyDimension;

    public:
      //beware: vme_spyBuffer calls readSpyStatusRegister on construction
      //Hense, create it when SpyBuffer is frozen already 
      //and expect it to read a register on the board via VME
      vme_spyBuffer(unsigned int status_addr, 
		    unsigned int base_addr,
		    VMEInterface * vmei);
      virtual ~vme_spyBuffer();

      //Set SpyBuffer size. Do not use, unless in desperate need. 
      //By default, it must be decoded from the SpyStatus word
      void setSpyDimension(unsigned int dim) { m_userSpyDimension = dim; }

      void read_spy_buffer();
      //base classes
      virtual int readSpyStatusRegister( uint32_t& spyStatus );
      virtual int readSpyBuffer() { read_spy_buffer(); return 0; };

      //reimplementation of base classes
      //FIXME allow to enforce particular spy buffer size
      virtual uint32_t getSpyDimension() {return m_userSpyDimension > 0  ? m_userSpyDimension : 
                                                                          SpyBuffer::getSpyDimension(); };
      
    };

  } // namespace ftk
} // namespace daq

#endif
