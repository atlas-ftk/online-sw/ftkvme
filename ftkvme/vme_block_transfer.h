#ifndef VME_BLOCK_TRANSFER
#define VME_BLOCK_TRANSFER

/*
 *  Functions used for VME block transfer
 */

#include <string>

#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "cmem_rcc/cmem_rcc.h"
#include "vme_rcc/vme_rcc.h"

//#define DEBUG_BLOCK
#define ROD_BUFFER_MAX_SIZE    0x10000   // Buffer dimension in bytes
#define ROD_DMATIMEOUT            4000	  // DMA timeout in ms 
#define DEF_VME_ADDR       "0x78000008"  // VME default address
#define DEF_OPERATION               "r"  // Default operation


namespace daq { 
namespace ftk {

  bool blockTransferPure(int vmeBaseAddress, u_int * memoryBaseAddress, u_int  numberOfWords, bool isRead);

  bool blockTransferReadFromFilePure(int vmeBaseAddress, int numberOfWords, std::string filePath, bool isRead);

  bool blockTransferReadFromFile	(int vmeBaseAddress, int numberOfWords, std::string filePath);

  bool blockTransferWriteToFile		(int vmeBaseAddress, int numberOfWords, std::string filePath);

  bool blockTransferRead(int vmeBaseAddress, u_int * memoryBaseAddress, int numberOfWords);

  bool blockTransferWrite(int vmeBaseAddress, u_int * memoryBaseAddress, int numberOfWords);

} // namespace ftk
} // namespace daq

#endif
