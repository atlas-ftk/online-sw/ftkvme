#ifndef STATUSREGISTERVMEDIRECT_H
#define STATUSREGISTERVMEDIRECT_H

#include "ftkcommon/StatusRegister/StatusRegister.h"
#include "ftkvme/VMEInterface.h"
#include "ftkvme/VMEManager.h"

#include <iostream>

// Namespaces
namespace daq {
namespace ftk {

/*! \brief Accesses the VME and reads out a single register
 */
class StatusRegisterVMEDirect
: public StatusRegister 
{
public:
	/*! \brief Class constructor
	 * 
	 * \param vmeSlot The VME slot of the board to read the register from
	 * \param fpgaNum The FPGA number to read the register from
	 * \param registerName The name of the register
	 * \param registerAddress The address of the register
	 * \param type The type of register to be read (left as an input to make modification later on easier)
	 */
	StatusRegisterVMEDirect(
		uint vmeSlot,
		uint fpgaNum,
		std::string registerName,
		uint registerAddress,
		srType type
	);
	~StatusRegisterVMEDirect();

	/*! \brief Reads the variable at the address passed to the constructor
	 */
	void readout();

private:
	VMEInterface *m_vme;
};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERVMEDIRECT_H */
