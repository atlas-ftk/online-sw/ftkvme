#ifndef VMEINTERFACE_H_
#define VMEINTERFACE_H_

#include "ers/ers.h"

#define ROD_BUFFER_MAX_SIZE 0x40000  // Buffer dimension in bytes                                  
#define ROD_DMATIMEOUT         4000  // DMA timeout in ms

/*!\brief Interface for communicating with an FPGA over VME.
 *
 * The connection is open using a base address that should point 
 * to the beggining of the address space associated to the FPGA.
 *
 * For example, to access Input 1 FPGA on an AUX card in slot 15,
 * open a connection with base address 0x79000000. Then to read
 * the xcvr control register, read address 0x30. The 0x79000000
 * should not be added to the address during the read operation.
 * 
 * If an operation fails, VMEException is thrown.
 */
class VMEInterface
{
public:
  /*! \brief Constructor
   *
   * \param disableBlockTransfers Set to true to emulate block transfers using single word transfers
   */
  VMEInterface(bool disableBlockTransfers=false);

  //! \brief Destructor, does nothing
  ~VMEInterface();

  /*! \brief Checks if the VME connection has been open.
   *
   * \return True if a valid connection is open, false otherwise.
   */
  bool is_open();

  /*! \brief Initialize a VME connection
   *
   * \param vmeaddr The base address of the FPGA
   * \param window_size The size of the window to open.
   */
  void open(u_int vmeaddr, u_int window_size);

  /*! \brief Disable block transfers
   *
   * \param value true if block transfers should be disabled
   */
  void disableBlockTransfers(bool value);

  /*! \brief Get the sblock transfer settings
   *
   * \return It returns true if block transfer is disabled else false
   */
  bool getDisableBlockTransfers();


  /*! \brief Read a single word
   *
   * \param address The address to access
   *
   * \return Value of the address
   */
  u_int read_word(u_int address);
  
  /*! \brief Write a single word
   *
   * \param address The target address
   * \param value The value to write
   */
  void write_word(u_int address, u_int value);

  /*! \brief Write and check a single word
   *
   * \param address The target address
   * \param value The value to write
   */
  void write_check_word(u_int address, u_int value);

  /*! \brief Read a single bit
   *
   * \param address The target address
   * \param bit The bit position to read
   *
   * \return The value of the bit in the address
   */
  bool read_bit(u_int address, u_int bit);
  
  /*! \brief Write a single bit
   *
   * Other bits in the address are left unchanged.
   *
   * \param address The target address
   * \param bit The bit position to update
   * \param value The value to write
   */
  void write_bit(u_int address, u_int bit, bool value);

  /*! \brief Write only a certain bits
   *
   * \param address The target address
   * \param value The value to write
   * \param mask Mask specifying bits to write
   */
  void write_mask(u_int address, u_int value, u_int mask);
  
  /*! \brief Read a block of data at some address
   *
   * \todo Implement block reads, currently emulated using single word reads
   *
   * \param address The start address of the block
   * \param nWords Number of words to read
   *
   * \return The data in the block
   */
  std::vector<u_int> read_block(u_int address, u_int nWords);

  /*! \brief Write a block of data to some address
   *
   * \todo Implement block writes, currently emulated using single word reads
   *
   * For single-word write emulation, the address is incremented by the software
   * for each word.
   *
   * \param address The start address of the block
   * \param data Data to write using a block read
   */
  void write_block(u_int address, const std::vector<u_int>& data);

  /*! \brief Read a single spy buffer
   *
   * \param buffer a vector of u_int to store spy buffer contents
   * \param spy_status_reg_VME_address address of status register
   * \param spyOverflow overflow flag
   * \param spyPointer last address written
   * \param spyDimension the size of the buffer (size = 1<<spyDimension)
   *
   */
  void read_spy_buffer(std::vector<u_int>& buffer,
		       u_int spy_status_reg_VME_address, 
		       u_int spy_RAM_VME_base_address, 
		       bool spyOverflow,
		       unsigned int spyPointer,
		       unsigned int spyDimension);

  /*! \brief Closes the VME connection
   */
  void close();

private:
  // Common VME variables
  int   m_status;
  u_int m_register_content;
  int   m_handle;
  u_int m_vmeaddr;

  bool m_disableBlockTransfers;

  /*! \brief Block read implementation.
   *
   * \todo implement
   *
   * \param address The start address of the block
   * \param nWords Number of words to read
   *
   * \return The data in the block
   */
  std::vector<u_int> read_block_blk(u_int address, u_int nWords);

  /*! \brief Block read emulation using single-word reads.
   *
   * \param address The start address of the block
   * \param nWords Number of words to read
   *
   * \return The data in the block
   */
  std::vector<u_int> read_block_sw(u_int address, u_int nWords);

  /*! \brief Block write implementation.
   *
   * \todo implement
   *
   * \param address The start address of the block
   * \param data Data to write using a block read
   */
  void write_block_blk(u_int address, const std::vector<u_int>& data);

  /*! \brief Block write emulation using single-word reads.
   *
   * The address is incremented by the software for each word.
   *
   * \param address The start address of the block
   * \param data Data to write using a block read
   */
  void write_block_sw(u_int address, const std::vector<u_int>& data);

 public:
  void write_block_from_cmem(u_int address, const unsigned long data,u_int size,u_int blocks=1);
  void read_block_to_cmem(u_int address, const unsigned long data,u_int size,u_int blocks=1);

  void deallocateCmem(int cmem_desc);

  int  allocateCmem(unsigned long  &cmem_desc_uaddr, unsigned long &ecmem_desc_paddr, unsigned long size);

};

#endif // VMEINTERFACE_H_
