#ifndef VMEMANAGER_H_
#define VMEMANAGER_H_

#include <ers/ers.h>

#include <map>

#include "ftkvme/VMEInterface.h"

/*! \brief Manages a set of open VME connections
 *
 * Disables real block writes by default. To enable, set the 
 * following environmental variable:
 *  export FTK_VME_REAL_BLOCK_WRITES=1
 */
class VMEManager
{
public:
  /*! \brief Access the global manager.
   */
  static VMEManager& global();

  /*! \brief Get VME interface to an AUX FPGA
   *
   * If a connection does not exist, it is crated.
   *
   * \param slot The board slot
   * \param fpga The target FPGA
   *
   * \return Instance of VMEInterface for VME operations
   */
  VMEInterface* aux(u_int slot, u_int fpga);
  VMEInterface* ssb(u_int slot, u_int fpga);
  VMEInterface* amb(u_int slot);
  VMEInterface* accessBoard(u_int slot, u_int fpga);
  VMEInterface* accessBoardSSB(u_int slot, u_int fpga);

private:
  VMEManager();
  VMEManager(VMEManager const&)     = delete;

  ~VMEManager();

  void operator=(VMEManager const&) = delete;

  std::map<std::pair<u_int, u_int>, VMEInterface* > _aux;
  std::map<std::pair<u_int, u_int>, VMEInterface* > _ssb;
  std::map<u_int, VMEInterface* > _amb;
  std::map<std::pair<u_int, u_int>, VMEInterface* > _board;

};

#endif // VMEMANAGER_H_
