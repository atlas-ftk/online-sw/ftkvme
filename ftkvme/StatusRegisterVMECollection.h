#ifndef STATUSREGISTERVMECOLLECTION_H
#define STATUSREGISTERVMECOLLECTION_H

#include "ftkcommon/StatusRegister/StatusRegisterCollection.h"
#include "ftkvme/StatusRegisterVMEDirect.h"
#include "ftkvme/StatusRegisterVMESelector.h"
#include "ftkvme/VMEInterface.h"
#include "ftkvme/VMEManager.h"

#include <iostream>
#include <sstream>

// Namespaces
namespace daq {
namespace ftk {

/*! \brief Stores and manages a collection of VMEDirect Status Register objects 
 */
class StatusRegisterVMECollection
: public StatusRegisterCollection
{
public:
	/*! \brief Class constructor
	 * 
	 * \param vmeSlot The VME slot of the board to read registers from
	 * \param fpgaNum The FPGA number to read registers from
	 * \param firstAddress The address of the first register to be read (see also selectorAddress description below)
	 * \param finalAddress The address of the final register to be read
	 * \param addrIncrement The spacing between the address numbers for the board being read from
	 * \param collectionName A descriptive name for the collection of registers
	 * \param collectionShortName A brief name for the collection of registers
	 * \param selectorAddress Selector address, default value is 0 
	 * (in the selector scheme you want to write into 'selectorAddress' values from 'firstAddress' and read the outcome from 'readerAddress')
 	 * \param readerAddress Reader access for selector, default value is 0 (see also selectorAddress description)
	 * \param type StatuRegister type (not used as it is hardhoded in StatusRegisterVMECollection::StatusRegisterVMECollection())
	 * \param access StatuRegister access type
	 */
	StatusRegisterVMECollection(
		uint vmeSlot,
		uint fpgaNum,
		uint firstAddress,
		uint finalAddress,
		uint addrIncrement,
		std::string collectionName,
		std::string collectionShortName,
                uint selectorAddress = 0,
                uint readerAddress = 0,
		srType type = srType::srOther,
                srAccess access = srAccess::VME
	);
	
	virtual ~StatusRegisterVMECollection();

private:

};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERVMECOLLECTION_H */
