// Functions used for spy buffer access using VME block transfer

#include "ftkvme/vme_spy_buffer.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

namespace daq { 
  namespace ftk {

    vme_spyBuffer::vme_spyBuffer(unsigned int status_addr, 
				 unsigned int base_addr,
				 VMEInterface *vmei): m_spy_status_reg_VME_address(status_addr),
						      m_spy_RAM_VME_base_address(base_addr),
						      m_userSpyDimension(0),
						      m_vme_interface(vmei) {
      readSpyStatusRegister(m_spyStatus);

    }

    //---------------------------------------------------

    vme_spyBuffer::~vme_spyBuffer(){}

    //---------------------------------------------------

    void vme_spyBuffer::read_spy_buffer(){
      
      m_vme_interface->read_spy_buffer(m_buffer,
				       m_spy_status_reg_VME_address,
				       m_spy_RAM_VME_base_address,
				       getSpyOverflow(),
				       getSpyPointer(),
				       getSpyDimension());

    }

    //---------------------------------------------------

    int vme_spyBuffer::readSpyStatusRegister( uint32_t& spyStatus ) {
        spyStatus = m_vme_interface->read_word(m_spy_status_reg_VME_address);
        return 0;
    }

  } // namespace ftk
} // namespace daq
