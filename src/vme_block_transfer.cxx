// Functions for VME transfer

#include "ftkvme/vme_block_transfer.h"

#include <sstream>
#include <fstream>
#include <iostream>

namespace daq {
namespace ftk {

bool blockTransferPure(int vmeBaseAddress, u_int * memoryBaseAddress, u_int  numberOfWords, bool isRead)
{
  // Return value for VME function error management
  VME_ErrorCode_t v_ret = 0;

  // Return valide for TimeStamp and CMEM error management
  u_int err_code = 0;  
  
  // Block Transfer list
  VME_BlockTransferList_t rlist;
  
  // CMEM variables
  int cmem_desc;
  unsigned long cmem_desc_uaddr, cmem_desc_paddr; // Logical and physical addresses of the RAM buffer
                                                  // used to read or write the blockstransfer
  volatile u_int* d_ptr;                          // Volatile pointer to access RAM buffer
  volatile u_int* mem_ptr;                        // Volatile pointer to access RAM buffer
   
  // Ratio words/bytes  
  unsigned int nByteToTransfer = numberOfWords * sizeof(int);	
 
  //////////////////////////////////////////////////////////////
  ////		Allocating the receiver buffer and Opening Vme address
  //////////////////////////////////////////////////////////////  
  err_code = CMEM_Open();
  if (err_code) 
    rcc_error_print(stdout, err_code);
 
  err_code = CMEM_SegmentAllocate(ROD_BUFFER_MAX_SIZE, const_cast<char*>("DMA_BUFFER"), &cmem_desc);
  if (err_code) 
    rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
  if (err_code)
    rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
  if (err_code)
    rcc_error_print(stdout, err_code);
 
  //v_ret = VME_Open();
  //if (v_ret != VME_SUCCESS)
  //  VME_ErrorPrint(v_ret);


  //////////////////////////////////////////////////////////////   
  ////   If writing 
  ///////////////////////////////////////////////////////////////
  if( ! isRead )
   {
  	d_ptr 	= (unsigned int*) cmem_desc_uaddr;
	  mem_ptr = (unsigned int*) memoryBaseAddress;

  	u_int offset = 0;
  	while( offset < numberOfWords)
  	{
		#ifdef DEBUG
		std::cout << "W : [0x"<< std::hex << mem_ptr+offset 
			  << "] " << mem_Ptr[offset] << std::endl;	
		#endif

                d_ptr[offset] = mem_ptr[offset];
		offset++;
	 }
  }

  
  //////////////////////////////////////////////////////////////   
  ////   Parameter dump
  ///////////////////////////////////////////////////////////////
  #ifdef DEBUG_BLOCK
  std::cout << "\nParameters: " << std::endl
		<< "Vme Address   = 0x"     << std::hex << vmeBaseAddress << std::endl
		<< "BufferAddress = 0x"     << std::hex << cmem_desc_paddr << std::endl
		<< "Parole trasm. = "       << std::dec << numberOfWords << std::endl 
    << "Bytes trasm.  = "       << std::dec << nByteToTransfer << std::endl;
  #endif	    

  //////////////////////////////////////////////////////////////   
  ////  BLOCK_TRANSFER  
  ///////////////////////////////////////////////////////////////  
  // Block trasfer preparation
  rlist.number_of_items = 1;
  rlist.list_of_items[0].vmebus_address       = vmeBaseAddress; // VME destination address 
  rlist.list_of_items[0].system_iobus_address = cmem_desc_paddr;// RAM address obtained from CMEM
  rlist.list_of_items[0].size_requested       = nByteToTransfer;

  if( isRead )  
	  rlist.list_of_items[0].control_word = VME_DMA_D32R;					
  else
	  rlist.list_of_items[0].control_word = VME_DMA_D32W;					
  
  
  // Calling Block transferBlock transfer 
  #ifdef DEBUG_BLOCK
  std::cout << "Calling Block transfer "<< std::endl; 
  #endif
  
  v_ret = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);
  
  #ifdef DEBUG_BLOCK
  std::cout << std::endl << "Block Transfer result: " << std::endl;
  #endif

  // Check result
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);      
  else
  {
	  #ifdef DEBUG_BLOCK
	  std::cout << "Success ! " << std::endl;
    #endif
  }

  //////////////////////////////////////////////////////////////   
  ////   Reading (iteration over the RAM buffer 
  ///////////////////////////////////////////////////////////////  
  if( isRead )
  {
  	d_ptr = (unsigned int*) cmem_desc_uaddr;
  
    std::cout << std::endl << "Reading: " << std::endl;
    
	  for ( unsigned int i = 0; i < ( numberOfWords ); i++)
		{
			d_ptr[i] = i;
			#ifdef DEBUG
			std::cout << "[" << std::dec << i << "] : 0x" << std::hex << d_ptr[i] << std::endl;
			#endif
		}
  }

  //////////////////////////////////////////////////////////////   
  ////   Close everything  
  ///////////////////////////////////////////////////////////////  
  #ifdef DEBUG_BLOCK
  std::cout << "Closing All " << std::endl;
  #endif

  //v_ret = VME_Close();
  //if (v_ret != VME_SUCCESS)
  //  VME_ErrorPrint(v_ret);

  err_code = CMEM_SegmentFree(cmem_desc);
  if (err_code)
    rcc_error_print(stdout, err_code);

  err_code = CMEM_Close();
  if (err_code)
    rcc_error_print(stdout, err_code);

  return true;
}

bool blockTransferReadFromFilePure(int vmeBaseAddress, int numberOfWords, std::string filePath, bool isRead)
{
  // Return value for VME function error management
  VME_ErrorCode_t v_ret = 0;

  // Return valide for TimeStamp and CMEM error management
  u_int err_code = 0;  
  
  // Block Transfer list
  VME_BlockTransferList_t rlist;
  
  // CMEM variables
  int cmem_desc;
  unsigned long cmem_desc_uaddr, cmem_desc_paddr; // Logical and physical addresses of the RAM buffer
                                                  // used to read or write the blockstransfer
  volatile u_int* d_ptr;                          // Volatile pointer to access RAM buffer
  
  // Ratio words/bytes  
  unsigned int nByteToTransfer = numberOfWords * sizeof(int);	

  std::fstream myfile;

  // store the file path
  myfile.open( filePath.c_str() );
  if ( myfile.fail() )
  {
	  // if impossible to open file return error
	  std::cerr << 	"\n\nError : Impossible to open file " << filePath << std::endl;
	  return false;
  }
  #ifdef DEBUG_BLOCK
  std::cerr << 	"Opened " << filePath << std::endl;
  #endif
  
  //////////////////////////////////////////////////////////////
  //// Allocating the receiver buffer   and Opening Vme address
  //////////////////////////////////////////////////////////////  
  err_code = CMEM_Open();
  if (err_code) 
    rcc_error_print(stdout, err_code);
  
  err_code = CMEM_SegmentAllocate(ROD_BUFFER_MAX_SIZE, const_cast<char*>("DMA_BUFFER"), &cmem_desc);
  if (err_code) 
    rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
  if (err_code)
    rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
  if (err_code)
    rcc_error_print(stdout, err_code);
 
  v_ret = VME_Open();
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);

  //////////////////////////////////////////////////////////////   
  ////   If writing
  ///////////////////////////////////////////////////////////////
  if( ! isRead )
  {
  	d_ptr = (unsigned int*) cmem_desc_uaddr;
	  std::string temp;
  	int offset = 0;
  	while( std::getline( myfile, temp )  && (offset < numberOfWords))
  	{
		  std::stringstream ss2;
  		int mynum;
		  ss2 << temp;
		  if( ( ss2 >> std::hex >> mynum ).fail() )
		  {
			  std::cerr << "\n\nFound a non number at line:" << offset << " \n\n from "  << std::endl;
			  return 1;
		  }
		  std::cout << "Writing " << mynum << std::endl; 
		
		  d_ptr[offset] = mynum;
		  offset++;
	  }
  }

  //////////////////////////////////////////////////////////////   
  ////   Parameters dump
  ///////////////////////////////////////////////////////////////
  #ifdef DEBUG_BLOCK
  std::cout << "\nParameters: " << std::endl
		<< "Vme Address   = 0x"     << std::hex << vmeBaseAddress << std::endl
		<< "BufferAddress = 0x"     << std::hex << cmem_desc_paddr << std::endl
		<< "Parole trasm. = "       << std::dec << numberOfWords << std::endl 
    << "Bytes trasm.  = "       << std::dec << nByteToTransfer << std::endl;
  #endif	    

  //////////////////////////////////////////////////////////////   
  ////  BLOCK_TRANSFER  
  ///////////////////////////////////////////////////////////////  
  // Preparation of the parameters
  rlist.number_of_items = 1;
  rlist.list_of_items[0].vmebus_address       = vmeBaseAddress;  // VME address destination
  rlist.list_of_items[0].system_iobus_address = cmem_desc_paddr; // RAM address obtained from CMEM
  rlist.list_of_items[0].size_requested       = nByteToTransfer;

  if( isRead )  
	  rlist.list_of_items[0].control_word = VME_DMA_D32R;					
  else
	  rlist.list_of_items[0].control_word = VME_DMA_D32W;					
  
  // Block transfer
  v_ret = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);
  
  #ifdef DEBUG_BLOCK
  std::cout << std::endl << "Block Transfer result: " << std::endl;
  #endif

  // check
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);      
  else
	{
	  #ifdef DEBUG_BLOCK
    std::cout << "Success ! " << std::endl;
	  #endif
	}

  //////////////////////////////////////////////////////////////   
  ////   Reading (iteration over the RAM buffer 
  ///////////////////////////////////////////////////////////////  
  if( isRead )
  {
  	d_ptr = (unsigned int*) cmem_desc_uaddr;
    std::cout << std::endl << "Reading: " << std::endl;
    
	  for ( int i = 0; i < ( numberOfWords ); i++)
		{
			d_ptr[i] = i;
			std::cout << "[" << std::dec << i << "] : 0x" << std::hex << d_ptr[i] << std::endl;
			myfile << "0x" << std::hex << d_ptr[i] << std::endl  ;
		}
  }

  //////////////////////////////////////////////////////////////   
  //// Close everything
  ///////////////////////////////////////////////////////////////  
  std::cout << "Closing All " << std::endl;

  v_ret = VME_Close();
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);

  err_code = CMEM_SegmentFree(cmem_desc);
  if (err_code)
    rcc_error_print(stdout, err_code);

  err_code = CMEM_Close();
  if (err_code)
    rcc_error_print(stdout, err_code);

  return true;
}

//
bool blockTransferReadFromFile	(int vmeBaseAddress, int numberOfWords, std::string filePath)
{
	return blockTransferReadFromFilePure(vmeBaseAddress, numberOfWords, filePath, true);
}

//
bool blockTransferWriteToFile		(int vmeBaseAddress, int numberOfWords, std::string filePath)
{
	return blockTransferReadFromFilePure(vmeBaseAddress, numberOfWords, filePath, false);
}

//
bool blockTransferRead(int vmeBaseAddress, u_int * memoryBaseAddress, int numberOfWords)
{
	return blockTransferPure(vmeBaseAddress, memoryBaseAddress, numberOfWords, true);
}

//
bool blockTransferWrite(int vmeBaseAddress, u_int * memoryBaseAddress, int numberOfWords)
{
	return blockTransferPure(vmeBaseAddress, memoryBaseAddress, numberOfWords, false);
}

} // Namespace ftk
} // Namespace daq
