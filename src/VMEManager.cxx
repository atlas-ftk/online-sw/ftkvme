#include "ftkvme/VMEManager.h"

VMEManager& VMEManager::global()
{
  static VMEManager instance;
  return instance;
}

VMEManager::VMEManager()
{ }

VMEManager::~VMEManager()
{
  for(auto& auxinstance : _aux)
    {
      auxinstance.second->close();
      delete auxinstance.second;
    }

  for(auto& auxinstance : _ssb)
    {
      auxinstance.second->close();
      delete auxinstance.second;
    }

  for(auto& ambinstance : _amb)
    {
      ambinstance.second->close();
      delete ambinstance.second;
    }

  for(auto& boardinstance : _board)
    {
      boardinstance.second->close();
      delete boardinstance.second;
    }
}

VMEInterface* VMEManager::aux(u_int slot, u_int fpga)
{
  VMEInterface* vme = VMEManager::global().accessBoard(slot, fpga);

  return vme;
}

VMEInterface* VMEManager::ssb(u_int slot, u_int fpga)
{
  VMEInterface* vme = VMEManager::global().accessBoardSSB(slot, fpga);

  return vme;
}

VMEInterface* VMEManager::amb(u_int slot)
{
  VMEInterface* vme = VMEManager::global().accessBoard(slot, 0);

  return vme;
}

VMEInterface* VMEManager::accessBoard(u_int slot, u_int fpga)
{
  std::pair<u_int, u_int> key=std::make_pair(slot,fpga);

  std::map<std::pair<u_int, u_int>, VMEInterface* >::iterator it = _board.find(key);
  if(it==_board.end())
    {
      u_int slotaddr = ((slot&0x1f) << 27);
      u_int fpgaaddr = ((fpga&0x1f) << 24);
      u_int vmeaddr = slotaddr + fpgaaddr;

      char *ena_c=std::getenv("FTK_VME_REAL_BLOCK_WRITES");
      std::string ena=(ena_c==NULL)?"0":ena_c;
      _board[key]=new VMEInterface(ena!="1");
      //_board[key]->open(vmeaddr,0x1000000);
      _board[key]->open(vmeaddr,0x5f0000);
    }

  return _board[key];	
}

VMEInterface* VMEManager::accessBoardSSB(u_int slot, u_int fpga)
{
  std::pair<u_int, u_int> key=std::make_pair(slot,fpga);

  std::map<std::pair<u_int, u_int>, VMEInterface* >::iterator it = _board.find(key);
  if(it==_board.end())
    {
      u_int slotaddr = ((slot&0x1f) << 27);
      u_int fpgaaddr = ((fpga&0x1f) << 12);
      u_int vmeaddr = slotaddr + fpgaaddr;

      char *ena_c=std::getenv("FTK_VME_REAL_BLOCK_WRITES");
      std::string ena=(ena_c==NULL)?"0":ena_c;
      _board[key]=new VMEInterface(ena!="1");
      _board[key]->open(vmeaddr,0x1000000);
    }

  return _board[key];	
}
