// functions for StatusRegisterVMEDirect.h

#include "ftkvme/StatusRegisterVMEDirect.h"

namespace daq {
namespace ftk {

// Public Member Functions:
StatusRegisterVMEDirect::StatusRegisterVMEDirect(
	uint vmeSlot,
	uint fpgaNum,
	std::string registerName,
	uint registerAddress,
	srType type
) {
	m_name = registerName;
	m_nameShort = registerName;
	m_address = registerAddress;
	m_type = type;

	// The VME Direct class only allows access via VME, so variables as set accordingly
	m_access = srAccess::VME;

	m_vme = VMEManager::global().accessBoard(vmeSlot, fpgaNum);
}

StatusRegisterVMEDirect::~StatusRegisterVMEDirect() {

}

void StatusRegisterVMEDirect::readout() {
	// Reads the value from the address passed to the constructor
	m_value = m_vme->read_word(m_address);
}

} // namespace ftk
} // namespace daq
