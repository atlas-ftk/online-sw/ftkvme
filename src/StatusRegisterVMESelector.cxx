// functions for StatusRegisterVMESelector.h

#include "ftkvme/StatusRegisterVMESelector.h"

namespace daq {
namespace ftk {

// Public Member Functions:
StatusRegisterVMESelector::StatusRegisterVMESelector(
	uint vmeSlot,
	uint fpgaNum,
	std::string registerName,
	uint registerAddress,
	srType type,
	uint selectorAddress,
	uint readerAddress
) {
	m_name = registerName;
	m_nameShort = registerName;
	m_address = readerAddress;
	m_type = type;

	m_writeAddress = selectorAddress;
	m_writeValue   = registerAddress;

	// The VME Selector class only allows access via VME_selector, so variables are set accordingly
	m_access = srAccess::VME_selector;

	m_vme = VMEManager::global().accessBoard(vmeSlot, fpgaNum);
}

StatusRegisterVMESelector::~StatusRegisterVMESelector() {

}

void StatusRegisterVMESelector::readout() {
	// AT THE MOMENT, THIS DOESN'T DO ANYTHING...
	// Reads the value from the address passed to the constructor
	//m_value = m_vme->read_word(m_address);
}

} // namespace ftk
} // namespace daq
