// functions for StatusRegisterVMEFactory.h
#include <memory>

#include "ftkvme/StatusRegisterVMEFactory.h"

#include "ftkcommon/exceptions.h"

namespace daq {
namespace ftk {

StatusRegisterVMEFactory::StatusRegisterVMEFactory(
	std::string boardName,
	u_int boardSlot,	std::string registersToRead,
	std::string isServerName
	) : StatusRegisterFactory (boardName, registersToRead, isServerName)
{
	// Store passed variables
	m_slot = boardSlot;
}

StatusRegisterVMEFactory::~StatusRegisterVMEFactory() {
	// delete any objects on top of the base StatusRegisterFactory
}

// Protected Member Functions
void StatusRegisterVMEFactory::setupCollection(
	char IDChar,
	uint fpgaNum,
	uint firstAddress, 
	uint finalAddress,
	uint addrIncrement,
	std::string collectionName, 
	std::string collectionShortName,
	std::vector<uint>* ISObjectVector,
	std::vector<uint>* ISObjectInfoVector,
	uint selectorAddress,
	uint readerAddress,
	srType type ,
        srAccess access
	) 
{
	// Checks if the input string contains the identifying character for the collection
  if (std::string::npos != m_inputString.find_first_of(toupper(IDChar))) {
    // If it does, a collection for the corresponding set of registers is created
    std::unique_ptr<StatusRegisterVMECollection> srVMEc = std::make_unique<StatusRegisterVMECollection>(m_slot,fpgaNum,firstAddress,finalAddress,addrIncrement,collectionName,collectionShortName,
						selectorAddress, readerAddress, type, access);
    m_collections.push_back(std::move(srVMEc));
    m_isObjectVectors.push_back(ISObjectVector);
    if(ISObjectInfoVector){
      setupCollectionInfo(ISObjectInfoVector, firstAddress, finalAddress, addrIncrement, fpgaNum, selectorAddress, readerAddress);
    }
  }
}

} // namespace ftk
} // namespace daq
