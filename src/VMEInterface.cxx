#include "ftkvme/VMEInterface.h"

#include "ftkcommon/core.h"
#include "ftkcommon/exceptions.h"

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "cmem_rcc/cmem_rcc.h"
#include <sstream>

VMEInterface::VMEInterface(bool disableBlockTransfers)
  : m_handle(0), m_disableBlockTransfers(disableBlockTransfers)
{ }

VMEInterface::~VMEInterface()
{ }

void VMEInterface::disableBlockTransfers(bool value)
{ m_disableBlockTransfers=value; }

bool VMEInterface::getDisableBlockTransfers()
{
  return m_disableBlockTransfers;
}

bool VMEInterface::is_open()
{ return m_handle!=0; }

void VMEInterface::open(u_int vmeaddr, u_int window_size)
{
  static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};

  m_vmeaddr=vmeaddr;

  err_str err_string;

  m_status = VME_Open();
  if (m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }

  master_map.vmebus_address   = vmeaddr;
  master_map.window_size      = window_size;
  master_map.address_modifier = VME_A32;
  master_map.options          = 0;
  m_status = VME_MasterMap(&master_map, &m_handle);
  if(m_status != VME_SUCCESS)
    {      
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }
}

u_int VMEInterface::read_word(u_int address)
{
  err_str err_string;

  m_status = VME_ReadSafeUInt(m_handle, address, &m_register_content);
  if(m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }
  return m_register_content;
}

void VMEInterface::write_word(u_int address, u_int value)
{
  err_str err_string;

  m_status = VME_WriteSafeUInt(m_handle, address, value);
  if(m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }
}

void VMEInterface::write_check_word(u_int address, u_int value)
{
  err_str err_string;

  m_status = VME_WriteSafeUInt(m_handle, address, value);
  if(m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }
  m_status = VME_ReadSafeUInt(m_handle, address, &m_register_content);
  if(m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }
  if(value != m_register_content)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }
}

bool VMEInterface::read_bit(u_int address, u_int bit)
{
  read_word(address);
  return (m_register_content>>bit)&0x1;
}

void VMEInterface::write_bit(u_int address, u_int bit, bool value)
{
  read_word(address);

  if(value)
    m_register_content|=(0x1<<bit);
  else
    m_register_content&=~(0x1<<bit);

  write_word(address,m_register_content);
}

void VMEInterface::write_mask(u_int address, u_int value, u_int mask)
{
  read_word(address);

  m_register_content=((value&mask)|(m_register_content&(~mask)));

  write_word(address,m_register_content);
}

std::vector<u_int> VMEInterface::read_block(u_int address, u_int nwords)
{
  if(m_disableBlockTransfers)
    return read_block_sw(address,nwords);
  else
    return read_block_blk(address,nwords);
}

void VMEInterface::write_block(u_int address, const std::vector<u_int>& data)
{
  if(m_disableBlockTransfers)
    write_block_sw(address,data);
  else
    write_block_blk(address,data);
}

void VMEInterface::read_spy_buffer(std::vector<u_int>& buffer,
				   u_int spy_status_reg_VME_address, 
				   u_int spy_RAM_VME_base_address, 
				   bool spyOverflow,
				   u_int spyPointer,
				   u_int spyDimension)
{
  buffer.reserve((1<<spyDimension));

  if(m_disableBlockTransfers)
    {
      switch (spyOverflow){
      case true :
	for(u_int i = spyPointer; i < (1<<spyDimension); ++i) {
	  read_word(spy_RAM_VME_base_address + (i<<2));
	  buffer.push_back(static_cast<unsigned int>(m_register_content));
	}

      case false :
	for(u_int i = 0; i < spyPointer; ++i) { // AMB case starts from 0
	//for(u_int i = 1; i < spyPointer; ++i) { // AUX case starts from 1
	  read_word(spy_RAM_VME_base_address + (i<<2));	  
	  buffer.push_back(static_cast<unsigned int>(m_register_content));
	}
      }
    }
  else if(!m_disableBlockTransfers)
    {      
      u_int err_code = 0;            // Return valid for TimeStamp and CMEM error management
      VME_BlockTransferList_t rlist; // Block Transfer list

      // CMEM variables
      int cmem_desc;
      unsigned long cmem_desc_uaddr, cmem_desc_paddr; // Logical and physical addresses of the RAM buffer
      volatile u_int* d_ptr;                          // Volatile pointer to access RAM buffer

      err_code = CMEM_Open();
      if (err_code) rcc_error_print(stdout, err_code);

      err_code = CMEM_SegmentAllocate(ROD_BUFFER_MAX_SIZE, const_cast<char*>("DMA_BUFFER"), &cmem_desc);
      if (err_code) rcc_error_print(stdout, err_code);

      err_code = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
      if (err_code) rcc_error_print(stdout, err_code);

      err_code = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
      if (err_code) rcc_error_print(stdout, err_code);

      // Add to vme list
      switch (spyOverflow){
      case true :
	
	rlist.list_of_items[0].vmebus_address       = spy_RAM_VME_base_address + (spyPointer<<2);     // VME address destination
	rlist.list_of_items[0].system_iobus_address = cmem_desc_paddr; // RAM address obtained from CMEM
	rlist.list_of_items[0].size_requested       = 4*(1<<spyDimension);
	rlist.list_of_items[0].control_word         = VME_DMA_D32R;
	rlist.number_of_items = 1;
	
	m_status = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);
	FTK_VME_ERROR( "- at line  "<<__LINE__<<" in "<<__FILE__<<" : Failure when calling the function 'VME_BlockTransfer'"  );

	d_ptr = (unsigned int*) cmem_desc_uaddr;

	for(u_int i = spyPointer; i < (1<<spyDimension); ++i)	
	  buffer.push_back(static_cast<unsigned int>(d_ptr[i]));
	
      case false :
	
	rlist.list_of_items[0].vmebus_address       = spy_RAM_VME_base_address + 0x4;     // VME address destination
	rlist.list_of_items[0].system_iobus_address = cmem_desc_paddr; // RAM address obtained from CMEM
	rlist.list_of_items[0].size_requested       = 4*spyPointer;
	rlist.list_of_items[0].control_word         = VME_DMA_D32R;
	rlist.number_of_items = 1;
	
	m_status = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);
	FTK_VME_ERROR( "- at line  "<<__LINE__<<" in "<<__FILE__<<" : Failure when calling the function 'VME_BlockTransfer'"  );

	d_ptr = (unsigned int*) cmem_desc_uaddr;
	
	for(u_int i = 1; i < spyPointer-1; ++i) // AUX case starts from 1
	  buffer.push_back(static_cast<unsigned int>(d_ptr[i]));
	  
      }
      // Free memory
      err_code = CMEM_SegmentFree(cmem_desc);
      if (err_code) rcc_error_print(stdout, err_code);
      ///
      err_code = CMEM_Close();
      if (err_code) rcc_error_print(stdout, err_code);
    }

}

void VMEInterface::write_block_from_cmem(u_int address, const unsigned long  data,u_int size,u_int blocks)
{
  //
  // Block write variables
  //
  err_str  err_string;           // Contains the error message
  VME_BlockTransferList_t rlist; // Block Transfer list
      
  // CMEM variables
  rlist.number_of_items=blocks;
  for(unsigned int i=0;i< blocks;i++) {
    rlist.list_of_items[i].vmebus_address       = m_vmeaddr + address; 
    rlist.list_of_items[i].system_iobus_address = data+i*size*4;
    rlist.list_of_items[i].size_requested       = size*4;  //hack for now     // Number of bytes
    rlist.list_of_items[i].control_word         = VME_DMA_D32W;        // WRITE!
  }

     
  //
  // The BLOCK transfer
  //
  

  m_status = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);

  if(m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }
}

void VMEInterface::read_block_to_cmem(u_int address, const unsigned long  data,u_int size,u_int blocks)
{
  //
  // Block write variables
  //
  err_str  err_string;           // Contains the error message
  VME_BlockTransferList_t rlist; // Block Transfer list
      
  // CMEM variables
  rlist.number_of_items=blocks;
  for(unsigned int i=0;i< blocks;i++) {
    rlist.list_of_items[i].vmebus_address       = m_vmeaddr + address; 
    rlist.list_of_items[i].system_iobus_address = data+i*size*4;
    rlist.list_of_items[i].size_requested       = size*4;  //hack for now     // Number of bytes
    rlist.list_of_items[i].control_word         = VME_DMA_D32R;        // WRITE!
  }

     
  //
  // The BLOCK transfer
  //
  

  m_status = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);

  if(m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }
}

int  VMEInterface::allocateCmem(unsigned long  &cmem_desc_uaddr, unsigned long &cmem_desc_paddr, unsigned long size) {

  int cmem_desc;
 err_type err_code = 0;         // Return valid for TimeStamp and CMEM error management
 err_str  err_string;           // Contains the error message
 err_code = CMEM_Open();
 if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }
 err_code = CMEM_BPASegmentAllocate(size, const_cast<char*>("DMA_BUFFER"), &cmem_desc);
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }
 err_code = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }
  
  err_code = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }
  return cmem_desc;
}
void VMEInterface::deallocateCmem(int cmem_desc ) {


 err_type err_code = 0;         // Return valid for TimeStamp and CMEM error management
 err_str  err_string;           // Contains the error message
// Free memory

  err_code = CMEM_BPASegmentFree(cmem_desc);
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }

  ///
  err_code = CMEM_Close();
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }

}


void VMEInterface::close()
{
  err_str err_string;

  m_status = VME_MasterUnmap(m_handle);
  if (m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }

  m_status = VME_Close();
  if (m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }

  m_handle=0;
}
			
std::vector<u_int> VMEInterface::read_block_blk(u_int address, u_int nwords)
{
  //
  // Block transfer variables
  //
  err_str err_string;
  err_type err_code = 0;         // Return valid for TimeStamp and CMEM error management
  VME_BlockTransferList_t rlist; // Block Transfer list
      
  // CMEM variables
  int cmem_desc;
  unsigned long cmem_desc_uaddr, cmem_desc_paddr; // Logical and physical addresses of the RAM buffer

  volatile u_int* d_ptr;                          // Volatile pointer to access RAM buffer
  //
  // Initialize buffer memory
  //
  err_code = CMEM_Open();
  if (err_code) rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentAllocate(ROD_BUFFER_MAX_SIZE, const_cast<char*>("DMA_BUFFER"), &cmem_desc);
  if (err_code) rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
  if (err_code) rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
  if (err_code) rcc_error_print(stdout, err_code);

  d_ptr = (unsigned int*) cmem_desc_uaddr;

  for(unsigned int i=0;i<nwords;i++) // CLEAR
    d_ptr[i]=0;

  // Add to vme list
  rlist.list_of_items[0].vmebus_address       = m_vmeaddr + address;     // VME address destination
  rlist.list_of_items[0].system_iobus_address = cmem_desc_paddr;      // RAM address obtained from CMEM
  rlist.list_of_items[0].size_requested       = nwords*4;
  rlist.list_of_items[0].control_word         = VME_DMA_D32R;                  // WRITE!

  //
  // The BLOCK transfer
  //
  rlist.number_of_items = 1;

  m_status = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);
  if(m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }

  d_ptr = (unsigned int*) cmem_desc_uaddr;
  std::vector<u_int> data;
  u_int word;
  for(u_int i=0;i<nwords;i++)
    {
      word=d_ptr[i];
      data.push_back(word);
    }

  //
  // Free memory
  //
  err_code = CMEM_SegmentFree(cmem_desc);
  if (err_code) rcc_error_print(stdout, err_code);

  ///
  err_code = CMEM_Close();
  if (err_code) rcc_error_print(stdout, err_code);

  return data;
}

std::vector<u_int> VMEInterface::read_block_sw(u_int address, u_int nwords)
{
  std::vector<u_int> data;

  for(u_int i=0; i<nwords; i++)
    {
      read_word(address + (i<<2));

      data.push_back(m_register_content);
    }
  
  return data;
}

void VMEInterface::write_block_blk(u_int address, const std::vector<u_int>& data)
{
  //
  // Block write variables
  //
  err_type err_code = 0;         // Return valid for TimeStamp and CMEM error management
  err_str  err_string;           // Contains the error message
  VME_BlockTransferList_t rlist; // Block Transfer list
      
  // CMEM variables
  int cmem_desc;
  unsigned long cmem_desc_uaddr, cmem_desc_paddr; // Logical and physical addresses of the RAM buffer

  volatile u_int* d_ptr;                          // Volatile pointer to access RAM buffer
  //
  // Initialize buffer memory
  //
  err_code = CMEM_Open();
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }

  err_code = CMEM_SegmentAllocate(ROD_BUFFER_MAX_SIZE, const_cast<char*>("DMA_BUFFER"), &cmem_desc);
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }

  err_code = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }

  err_code = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }

  d_ptr = (unsigned int*) cmem_desc_uaddr;

  for(u_int i=0;i<data.size();i++)
    d_ptr[i]=data[i];

  // Add to vme list
  rlist.list_of_items[0].vmebus_address       = m_vmeaddr + address; // VME address destination
  rlist.list_of_items[0].system_iobus_address = cmem_desc_paddr;     // RAM address obtained from CMEM
  rlist.list_of_items[0].size_requested       = data.size()*4;       // Number of bytes
  rlist.list_of_items[0].control_word         = VME_DMA_D32W;        // WRITE!
      
  //
  // The BLOCK transfer
  //
  rlist.number_of_items = 1;

  m_status = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);
  if(m_status != VME_SUCCESS)
    {
      VME_ErrorString(m_status, err_string);
      daq::ftk::VmeError vmeerror(ERS_HERE,err_string);
      throw vmeerror;
    }

  // Free memory
  err_code = CMEM_SegmentFree(cmem_desc);
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }

  ///
  err_code = CMEM_Close();
  if(err_code)
    {
      rcc_error_string(err_string,err_code);
      daq::ftk::CMEMError cmemerror(ERS_HERE,err_string);
      throw cmemerror;
    }

}

void VMEInterface::write_block_sw(u_int address, const std::vector<u_int>& data)
{
  for(u_int i=0; i<data.size(); i++)
    write_word(address + (i<<2), data[i]);
}
