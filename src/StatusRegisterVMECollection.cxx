// functions for StatusRegisterVMECollection.h

#include "ftkvme/StatusRegisterVMECollection.h"

namespace daq {
namespace ftk {

StatusRegisterVMECollection::StatusRegisterVMECollection(
	uint vmeSlot,
	uint fpgaNum,
	uint firstAddress,
	uint finalAddress,
	uint addrIncrement,
	std::string collectionName,
	std::string collectionShortName,
	uint selectorAddress,
	uint readerAddress,
	srType type,
	srAccess access
	) : StatusRegisterCollection (type, access) 
{
	setName(collectionName);
	setNameShort(collectionShortName);

	// Loop through addresses, creating a SRVMEDirect object for each
	for (uint addr = firstAddress; addr <= finalAddress; addr+=addrIncrement) {
		// Name is set to the address in Hex, to avoid having to provide a list of names for each register.
		std::stringstream buffer;
		buffer << std::hex << addr;
		
		// Create a SR object for each address
		std::unique_ptr<StatusRegister> srVMEd;
		//Direct or selector access
		if( access == srAccess::VME || access == srAccess::VME_direct || access == srAccess::dummy )
			srVMEd = std::make_unique<StatusRegisterVMEDirect>(vmeSlot, fpgaNum, buffer.str(), addr, type);
		else if( access == srAccess::VME_selector )  {
			buffer << "_" << std::hex << selectorAddress << "_" << std::hex << readerAddress;
			srVMEd = std::make_unique<StatusRegisterVMESelector>(vmeSlot, fpgaNum, buffer.str(), addr, type, selectorAddress, readerAddress);
		}
		// Add the object to the SR collection
		if( srVMEd )
			addStatusRegister(std::move(srVMEd));
	}
}

StatusRegisterVMECollection::~StatusRegisterVMECollection() {
	// The StatusRegisterCollection deconstructor deletes the StatusRegisters stored in m_registers, so no need to do it here
}

} // namespace ftk
} // namespace daq
